package com.poc.security.repo;

import com.poc.security.entities.UserModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserModelRepo extends MongoRepository<UserModel,String > {
    UserModel findByUsername(String username);
}
