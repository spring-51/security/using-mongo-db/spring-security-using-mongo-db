package com.poc.security.controller;

import com.poc.security.dto.LoginRequest;
import com.poc.security.entities.UserModel;
import com.poc.security.repo.UserModelRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/public")
public class PublicController {

    @Autowired
    private UserModelRepo repo;

    @Autowired
    private AuthenticationManager authenticationManager;

    @PostMapping(value = "/login")
    public String login(@RequestBody LoginRequest request){
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(request.getUsername(),request.getPassword())
        );
        return "success";
    }

    @PostMapping(value = "/users")
    public UserModel saveUser(@RequestBody LoginRequest request){
        return repo.save(UserModel.builder()
                .username(request.getUsername())
                .password(request.getPassword())
                .build());
    }
}
