package com.poc.security.service;

import com.poc.security.entities.UserModel;
import com.poc.security.repo.UserModelRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class UserModelService implements UserDetailsService // spring sec specific class
{
    @Autowired
    private UserModelRepo repo;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        UserModel userModel = repo.findByUsername(s);
        if(userModel != null){
            String username = userModel.getUsername();
            String password = userModel.getPassword();
            return new User(username, password, new ArrayList<>());
        }
        return null ;
    }
}
