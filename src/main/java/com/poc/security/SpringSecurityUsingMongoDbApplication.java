package com.poc.security;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSecurityUsingMongoDbApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringSecurityUsingMongoDbApplication.class, args);
	}

}
