package com.poc.security.entities;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Getter
@Setter
@Builder
@Document(collection = "users")
public class UserModel {
    @Id
    private String id;

    private String username;
    private String password;
}
